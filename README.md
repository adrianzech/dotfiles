# dotfiles

![alt Screenshot](https://raw.githubusercontent.com/adrianzech/dotfiles/master/screenshots/main.png)

- **Window Manager**: i3-gaps
- **Bar**: Polybar
- **Shell**: zsh
-  **Terminal**: Termite
- **Compositor**: Compton
- **Menu**: Rofi
- **Theme**: Custom Blue Theme

## Install

Run this:

```sh
git clone https://github.com/adrianzech/dotfiles.git ~/.dotfiles
~/.dotfiles/install
```

This will symlink the appropriate files in `.dotfiles` to your home directory.
